import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SignUp from '../screens/SignUp';
import Login from '../screens/Login';
import Main from './AppBottomTabs';
import SeeEvent from '../screens/SeeEvent';
import SeeEventDetail from '../screens/SeeEventDetail';
import EditProfile from '../screens/EditProfile';
import EditPassword from '../screens/EditPassword';
import CreateEvent from '../screens/CreateEvent';
import UpdateEvent from '../screens/UpdateEvent';
import SavedEvent from '../screens/SavedEvent';

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        headerTintColor: '#FFFFFF',
        headerStyle: {backgroundColor: '#214457'},
      }}>
      <Stack.Screen name="See Event" component={SeeEvent} />
      <Stack.Screen
        options={{headerShown: true}}
        name="Sign Up"
        component={SignUp}
      />
      <Stack.Screen
        options={{headerShown: true}}
        name="Sign In"
        component={Login}
      />
      <Stack.Screen name="Main" component={Main} />
      <Stack.Screen
        options={{headerShown: true}}
        name="SeeEvent Detail"
        component={SeeEventDetail}
      />
      <Stack.Screen name="Edit Profile" component={EditProfile} />
      <Stack.Screen name="Edit Password" component={EditPassword} />
      <Stack.Screen name="Create Event" component={CreateEvent} />
      <Stack.Screen name="Update Event" component={UpdateEvent} />
      <Stack.Screen name="Saved Event" component={SavedEvent} />
    </Stack.Navigator>
  );
};

export default AppStack;
