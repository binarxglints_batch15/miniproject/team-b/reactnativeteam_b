import React from 'react';
import {Image, Text} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import Explore from '../screens/Explore';
import MyEvent from '../screens/MyEvent';
import Profile from '../screens/Profile';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SeeEventIcon from '../assets/images/SeeEventIcon.png';

const Tab = createBottomTabNavigator();

const AppBottomTabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: true,
        headerTintColor: '#FFFFFF',
        headerStyle: {backgroundColor: '#214457'},
        headerTitleAlign: 'center',
        tabBarShowLabel: true,
        tabBarInactiveTintColor: '#999999',
        tabBarActiveTintColor: '#214457',
        tabBarInactiveBackgroundColor: '#FFFFFF',
        tabBarActiveBackgroundColor: '#ECEEEF',
        tabBarLabelStyle: {fontSize: 12, fontWeight: '700'},
        tabBarIcon: ({focused, size, color}) => {
          let iconName;
          if (route.name == 'Home') {
            iconName = focused ? 'home' : 'home-outline';
            size = focused ? 30 : 25;
          } else if (route.name == 'Explore') {
            iconName = focused ? 'search' : 'search-outline';
            size = focused ? 30 : 25;
          } else if (route.name == 'My Event') {
            iconName = focused ? 'ios-newspaper' : 'ios-newspaper-outline';
            size = focused ? 30 : 25;
          } else if (route.name == 'Profile') {
            iconName = focused ? 'person' : 'person-outline';
            size = focused ? 30 : 25;
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}>
      <Tab.Screen
        options={{
          headerTitle: 'SeeEvent',
          headerTintColor: '#ECEEEF',
          headerTitleStyle: {fontStyle: 'italic'},
        }}
        name="Home"
        component={Home}
      />
      <Tab.Screen
        options={{headerShown: false}}
        name="Explore"
        component={Explore}
      />
      <Tab.Screen
        options={{
          headerTitle: 'SeeEvent',
          headerTintColor: '#ECEEEF',
          headerTitleStyle: {fontStyle: 'italic'},
          // tabBarIcon: ({focused}) => {
          //   return (
          //     <Image style={{width: 30, height: 28}} source={SeeEventIcon} />
          //   );
          // },
        }}
        name="My Event"
        component={MyEvent}
      />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

export default AppBottomTabs;
