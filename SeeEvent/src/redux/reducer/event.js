const initialState = {
  dataEvent: [],
  dataEventDetail: [],
  loading: false,
};

const event = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_EVENT':
      return {
        ...state,
        loading: true,
      };
    case 'GET_EVENT_SUCCESS':
      return {
        ...state,
        loading: false,
        dataEvent: action.data,
      };
    case 'GET_EVENT_DETAIL':
      return {
        ...state,
        loading: true,
      };
    case 'GET_EVENT_DETAIL_SUCCESS':
      return {
        ...state,
        loading: false,
        dataEventDetail: action.data,
      };

    default:
      return state;
  }
};

export default event;
