import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reducer/event';

import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga/eventSaga';

const sagaMiddleware = createSagaMiddleware();

const storeRedux = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default storeRedux;
