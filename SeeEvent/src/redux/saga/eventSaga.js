import {takeLatest, put} from 'redux-saga/effects';
import axios from 'axios';

function* getEvent() {
  try {
    const responseEvent = yield axios.get(``);

    yield put({type: 'GET_EVENT_SUCCESS', data: responseEvent.data});
  } catch (err) {
    console.log(err);
  }
}

function* getEventDetail(eventId) {
  try {
    const responseEvent = yield axios.get(``);

    yield put({type: 'GET_EVENT_DETAIL_SUCCESS', data: responseEvent.data});
  } catch (err) {
    console.log(err);
  }
}

function* eventSaga() {
  yield takeLatest('GET_EVENT', getEvent);
  yield takeLatest('GET_EVENT_DETAIL', getEventDetail);
}

export default eventSaga;
