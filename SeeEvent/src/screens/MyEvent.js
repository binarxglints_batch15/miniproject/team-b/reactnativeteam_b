import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const MyEvent = () => {
  return (
    <View style={styles.container}>
      <Text>My Event</Text>
    </View>
  );
};

export default MyEvent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    backgroundColor: '#ECEEEF',
  },
});
