import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import SeeEventImg from '../assets/images/SeeEvent.png';
import SeeEventIcon from '../assets/images/SeeEventIcon.png';

const SeeEvent = props => {
  return (
    <View style={styles.container}>
      <Image source={SeeEventImg} style={styles.seeEventImg} />
      <View style={styles.titleContainer}>
        <Image source={SeeEventIcon} style={styles.seeEventIcon} />
        <Text style={styles.title}>SeeEvent</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          props.navigation.navigate('Sign Up');
        }}>
        <Text style={styles.buttonText}>Sign Up</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonSignIn}
        onPress={() => {
          props.navigation.navigate('Sign In');
        }}>
        <Text style={styles.buttonSignInText}>Sign In</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.guest}>
        <Text style={styles.guestText}>Continue as guest</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SeeEvent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#ECEEEF',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  seeEventIcon: {
    width: 56,
    height: 40,
  },
  seeEventImg: {
    width: 240,
    height: 194,
    marginBottom: 50,
  },
  title: {
    fontSize: 32,
    fontStyle: 'italic',
    fontWeight: '700',
    fontFamily: 'noto-sans.italic.ttf',
    color: '#214457',
    marginLeft: 5,
  },
  formInput: {
    width: '95%',
  },
  inputStyle: {
    fontSize: 18,
    color: '#214457',
  },
  inputContainerStyle: {
    height: 48,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#999999',
    paddingHorizontal: 8,
    marginBottom: -5,
    backgroundColor: '#FFFFFF',
  },
  icon: {
    fontSize: 22,
    color: '#999999',
  },
  button: {
    width: '90%',
    height: 48,
    backgroundColor: '#214457',
    borderWidth: 2,
    borderRadius: 10,
    borderColor: '#214457',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 7,
  },
  buttonSignIn: {
    width: '90%',
    height: 48,
    backgroundColor: '#ECEEEF',
    borderWidth: 2,
    borderRadius: 10,
    borderColor: '#214457',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 7,
  },
  buttonText: {
    fontSize: 16,
    color: '#FFFFFF',
    fontWeight: '700',
  },
  buttonSignInText: {
    fontSize: 16,
    color: '#214457',
    fontWeight: '700',
  },
  guest: {
    marginTop: 15,
  },
  guestText: {
    fontSize: 16,
    fontWeight: '700',
    color: '#3E89AE',
  },
});
