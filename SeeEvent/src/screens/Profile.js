import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Profile = props => {
  return (
    <View>
      <Text>Profile</Text>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    backgroundColor: '#ECEEEF',
  },
});
