import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const SeeEventDetail = props => {
  return (
    <View style={styles.container}>
      <Text>SeeEvent Detail</Text>
    </View>
  );
};

export default SeeEventDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    backgroundColor: '#ECEEEF',
  },
});
