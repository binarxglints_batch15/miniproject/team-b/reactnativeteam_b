import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Input} from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const SignUp = props => {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [pass, setPass] = useState();
  const [confirmPass, setConfirmPass] = useState();
  const [hidePass, setHidePass] = useState(true);
  const [hideConfirmPass, setHideConfirmPass] = useState(true);

  const dataRegister = {
    firstName,
    lastName,
    email,
    pass,
    confirmPass,
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Join us!</Text>
      </View>
      <View style={styles.formInput}>
        <Input
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
          placeholder="First Name"
          placeholderTextColor="#999999"
          value={firstName}
          onChangeText={value => setFirstName(value)}
          rightIcon={<Ionicons name="person" style={styles.icon} />}
        />
        <Input
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
          placeholder="Last Name"
          placeholderTextColor="#999999"
          value={lastName}
          onChangeText={value => setLastName(value)}
          rightIcon={<Ionicons name="person" style={styles.icon} />}
        />
        <Input
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
          placeholder="Email"
          placeholderTextColor="#999999"
          value={email}
          onChangeText={value => setEmail(value)}
          rightIcon={<MCIcons name="email" style={styles.icon} />}
        />
        <Input
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
          placeholder="Password"
          placeholderTextColor="#999999"
          secureTextEntry={hidePass ? true : false}
          value={pass}
          onChangeText={value => setPass(value)}
          rightIcon={
            <FontAwesome
              name={hidePass ? 'eye-slash' : 'eye'}
              style={styles.icon}
              onPress={() => setHidePass(!hidePass)}
            />
          }
        />
        <Input
          inputStyle={styles.inputStyle}
          inputContainerStyle={styles.inputContainerStyle}
          placeholder="Confirm Password"
          placeholderTextColor="#999999"
          secureTextEntry={hideConfirmPass ? true : false}
          value={confirmPass}
          onChangeText={value => setConfirmPass(value)}
          rightIcon={
            <FontAwesome
              name={hideConfirmPass ? 'eye-slash' : 'eye'}
              style={styles.icon}
              onPress={() => setConfirmPass(!hideConfirmPass)}
            />
          }
        />
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          props.navigation.navigate('Main', {screen: 'Home'});
        }}>
        <Text style={styles.buttonText}>Sign Up</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.issue}>
        <Text style={styles.issueText}>Having issue when signup?</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#ECEEEF',
  },
  title: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 40,
    fontWeight: '700',
    fontFamily: 'noto-sans.italic.ttf',
    color: '#214457',
    marginBottom: 20,
  },
  formInput: {
    width: '95%',
  },
  inputStyle: {
    fontSize: 18,
    color: '#214457',
  },
  inputContainerStyle: {
    height: 48,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#999999',
    paddingHorizontal: 8,
    marginBottom: -5,
    backgroundColor: '#FFFFFF',
  },
  icon: {
    fontSize: 22,
    color: '#999999',
  },
  button: {
    width: '90%',
    height: 48,
    backgroundColor: '#214457',
    borderWidth: 2,
    borderRadius: 10,
    borderColor: '#214457',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 7,
  },
  buttonText: {
    fontSize: 16,
    color: '#FFFFFF',
    fontWeight: '700',
  },
  issue: {
    marginBottom: 15,
  },
  issueText: {
    fontSize: 16,
    fontWeight: '700',
    color: '#3E89AE',
  },
});

export default SignUp;
